#!/bin/bash

outdir=$1
while read line; do
	path=${line%.*}
	file=${line##*/}
	mkdir -p "$path"
	cat <<EOF > "$path"/index.html
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="refresh" content="0; url='../$file'" />
	</head>
	<body>
		<p>Please follow <a href="../$file">this link</a>.</p>
	</body>
</html>
EOF

done < <(find $outdir -type f -iname "*.html")
