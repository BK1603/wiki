=======
Nouveau
=======

The **nouveau** project aims to build high-quality, free/libre software drivers
for NVIDIA cards. "Nouveau" [nuvo] is the French word for "new". Nouveau is
composed of a Linux kernel KMS driver (nouveau), Gallium3D drivers in Mesa, and
the Xorg DDX (xf86-video-nouveau). The kernel components have also been ported
to NetBSD.


Community
=========

The nouveau community is present on mailing lists and IRC. The `nouveau mailing
list`_ is for general nouveau discussions. For developers, there is also
nouveau-related discussions on the `dri-devel mailing list`_ and the `mesa-dev
mailing list`_.

The ``#nouveau`` IRC channel on `FreeNode`_ is also available for nouveau
discussion and `logs`_ are available.

The `issue tracker`_ and `this documentation`_ is available on the
`freedesktop.org GitLab`_.


.. _FreeNode: https://freenode.net/
.. _logs: https://people.freedesktop.org/~cbrill/dri-log/index.php?channel=nouveau
.. _nouveau mailing list: https://lists.freedesktop.org/mailman/listinfo/nouveau
.. _dri-devel mailing list: https://lists.freedesktop.org/mailman/listinfo/dri-devel
.. _mesa-dev mailing list: https://lists.freedesktop.org/mailman/listinfo/mesa-dev
.. _issue tracker: https://gitlab.freedesktop.org/drm/nouveau/-/issues
.. _this documentation: https://gitlab.freedesktop.org/nouveau/wiki
.. _freedesktop.org GitLab: https://gitlab.freedesktop.org/nouveau
