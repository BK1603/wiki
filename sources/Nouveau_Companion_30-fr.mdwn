[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_30]]/[[ES|Nouveau_Companion_30-es]]/[[FR|Nouveau_Companion_30-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 11 Novembre 2007


### Introduction

Et une fois de plus, plus de 2 semaines se sont passées et le temps est à nouveau venu pour une autre édition du TiNDC. 

Donc nous nous battons actuellement pour publier le plus tôt possible notre première version « stable » de Nouveau. Cette sortie incluera la 2D, Xv et l'accélération EXA pour toutes les cartes du NV05 au NV4x. Les possesseurs de NV04 n'auront pas EXA à cause des limitations matérielles, pendant que les NV5x (GeForce 8x00) n'auront pas plus qu'une 2D fonctionnelle et espérons-le un _modesetting_ qui marche pour la plupart des cas. 

NV5x est sérieusement en sous-effectif, avec seulement Darktama (qui est limité par le temps à cause d'affaires dans la vraie vie) et deux développeurs novices qui essayent de comprendre le fonctionnement. KoalaBR est limité à quelques tests et à du _reverse engineering_ également à cause d'affaires dans la vraie vie. 

Nous avons souvent des retours comme quoi notre driver est lent avec GNOME ou KDE dans le sens que le rendu des icônes semble être retardé. Tout les cas que nous avons eu jusqu'à maintenant se passaient sur un xserver 1.3. Mettre à jour en xserver 1.4.1 résous le problème. N'utilisez pas xserver 1.4 qui avait quelques problèmes avec EXA et dans la gestion des entrées. 

En passant, pour chaque nouvelle édition, j'ennuie plusieurs développeurs pour vérifier qu'il n'y a pas d'erreurs ou de fautes de frappe. J'aimerais leur dire « Merci » de leur aide. 


### Statut actuel

Marcheu a résolu les problèmes les plus importants avec EXA sur NV3x et a même fait refonctionner sur PPC. Mais il y a encore des bugs avec composite, dont un qui fait disparaître le texte dans les applications Qt. 

La prochaine chose que nous souhaitons faire, c'est dire « Merci » à [[IronPeter|IronPeter]], qui travaille à faire fonctionner la 3D sur PS3 (une machine big endian). Il a demandé de l'aide et, nous sommes ravis d'avoir pu aider autant que possible. Donc il a réussi à faire fonctionner des pixel shaders et des fragment shaders. Mais la meilleure chose arrive : il a également trouvé notre problème avec les couleurs manquantes de glxgear : les fragment shaders ont besoin que les constantes 32 bits soient échangées par un _byteswap_ 16 bits (ABCD devenant CDAB). Darktama a immédiatement changé notre code dans DDX pour que ça corresponde ; airlied a effectué quelques tests sur son G5 et ça fonctionnait. Donc EXA sur NV4x est maintenant aussi accéléré sur PPC ! 

En voyant ça, airlied a également ajouté la correction de bug au driver DRI. Et il s'est débarassé des engrenages "sans couleurs". 

Cependant, plus tard, des tests sur différents matériels étaient mitigés pour la 2D : quelques-uns disaient que ça marchait, d'autres que ça plantait.  
 [[Blog de Airlied|http://airlied.livejournal.com/52707.html]]  
 [[IRC|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-11-01.htm#0758]]  
 [[patch DRI|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ee793281b221415f794af6aadaa9764023612e0b]]  
 [[patch DDX 1|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=f59e596bcd90ef824cd41e0c37952e574d6914bb]] [[patch DRI  2|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=5bd8ba52788b3b3b8f91ba59c29e154e38482481]] 

Toujours est-il que l'EXA non-NV4x ne marchait toujours pas sur PPC, ou lentement. Donc Airlied et marcheur ont tout les deux contacté le _guru_ PPC benh et lui ont demandé de l'aide. Il a bien voulu jeter un œil au code et l'auditer pour des bugs classiques d' _endianness_. Ce travail est encore en cours, mais au moins nous n'avons pas commis de crime PPC évident : même benh a besoin de regarder en profondeur. 

AndrewR a testé EXA sur NV11 et a rapporté le même problème (polices manquantes) sur NV11. p0g a enquêté sur le problème et s'est fait embrouillé par les valeurs des positions X/Y à l'écran envoyées à Nouveau par EXA. Plus tard, il a trouvé le problème et l'a résolu. 

Ahuillet et p0g ont essayé de passer le dernier obstacle pour avoir un EXA rapide sur NV1x : trouver un moyen d'implémenter le _[[PictOp|PictOp]]_ A8+A8. Cela s'est prouvé être difficile, puisqu'ils n'ont pas trouvé de méthode matérielle pour le faire. Plusieurs options ont été étudiées (comme convertir en un format de texture connu, où faire la conversion, etc.), mais aucune solution immédiate n'en est ressortie. Après quelques tests qui ont fait apparaître des problèmes si la largeur ou si les positions x/y n'étaient pas paires, une discussion entre ahuillet, p0g et marcheu a commencé. Les bidouilles de ahuillet ont été abandonnées et un patch d'amélioration de EXA fut esquissé. [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-07#1848]] 

Ils ont tout les deux essayé différentes valeurs alors inconnues pour les formats de texture afin de trouver un mode de texture qui convienne, mais encore une fois sans résultat. 

Pmdata et CareyM ont trouvé des bugs dans Renouveau, qui provoquaient des résultats incorrects/erronés pour certains tests. CareyM est même revenu à des versions de drivers en 71xx pour s'assurer que ça n'était pas un bug dans le driver. (Merci beaucoup pour ce gros travail !) Plus tard pmdata a trouvé le coupable : un Enable(GL_TEXTURE_RECTANGLE_NV) là où il ne fallait pas. Il a réécrit le test pour supprimer ce bug. Le patch est  [[ici|http://nouveau.cvs.sourceforge.net/nouveau/renouveau/tests.c?r1=1.216&r2=1.217]]. 

stillunknown ainsi que divers contributeurs ont essayé de mettre en forme le _mode switching_. chowmeined avait des problèmes avec des timings incorrects qui laissaient deux lignes roses en bas et sur le côté droit de l'affichage. Il a réussi à éliminer celles du bas mais n'a pas réussi à se débarasser de celles sur la droite. Stillunknown a testé les patchs proposés et les a fait fonctionner. 

Plus tard, malc0 est arrivé avec un patch pour parser le DCB depuis le BIOS de la carte graphique. De plus, le DDX a subi beaucoup de nettoyages au niveau du code randr 1.2 et au code qui y est lié, afin de faciliter le développement et la maintenance. 
[[!table header="no" class="mointable" data="""
Note: _Display Configuration Block_ (DCB)  
 Le DCB est une partie du BIOS de la cartes graphique et consiste en un certain nombre d'entrées qui incluent de données intéressantes pour le _mode setting_. Chaque entrée est liée à une sortie potentielle (possible) comme VGA, TV-OUT, etc. Pour l'instant, Nouveau utilise les données associées avec les entrées pour savoir quels types de sorties sont présentes, quels ports I2C utiliser pour récupérer les données EDID pour chaque sortie, et comment le shéma de routage RAMDAC doit être effectué. 
"""]]

Mais plus tard, stillunknown a réussi à initialiser à froid la deuxième sortie avec randr 1.2, ce qui fait donc un problème majeur en moins. [[patch|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=8241710c94f6df0bf683bc3c93f7ea1ca14d118c]]   
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-06#1030]] 

pq a fait quelques tests sur NV20 et a fourni plus d'informations à propos de ses résultats. En jouant avec plusieurs _timing registers_ ils ont tout les deux réussi à ce que ça marche également sur NV20. [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2042]] 

jb17some a présenté son travail de _reverse engineering_ sur XvMC. Il semble être sûr de comprendre maintenant le fonctionnement. La FIFO est configurée et contrôle le rendu comme d'habitude, mais les données ne sont pas envoyées par la FIFO mais via un buffer séparé. Ce buffer est créé/mis à jour par des appels à XVMCCreateContext(), XVMCCreateSurface() et XVMCRenderSurface(). Il reste encore à voir s'il a l'intention de tenter une implémentation ou pas. [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2232]] 

Avec le travail sur NV3x terminé et marcheu corrigeant les bugs restant exclusivement liés à NV30, kolb s'est tourné vers un nouveau challenge. Après avoir parlé à marcheu, airlied et darktama, il a décidé de travailler un peu sur Gallium et TTM. JKolb a effectué quelques commits, d'abord le squelette depuis le driver _BO_ sous forme de fonctions vides, puis a complété par du code du TTM radeon de airlied. À ce moment, darktama a envoyé à jkolb un diff de sa branche TTM, et il a ajouté quelques bouts de ce code qui manquaient depuis le driver _BO_, et a committé le driver _fence_ de darktama. Darktama a alors un peu travaillé pour régler les quelques bugs qui ont été trouvés par rapport au code original. 

Pour avoir un _framework_ Gallium3D/TTM complété, il manque encore breaucoup. Le DRM a encore besoin de morceux importants ("superioctl", quelques mauvais bugs, etc etc.), et nous avons tout le code en _userspace_ à adapter. C'est vraiment un gros travail ! 

En passant : airlied a envoyé le nouveau DRM incluant TTM à la LKML. Ça veut dire que l'API sera sans doute finie et assez stable pour pouvoir être intégrée au noyau Linux. [[http://www.uwsg.iu.edu/hypermail/linux/kernel/0711.0/1178.html|http://www.uwsg.iu.edu/hypermail/linux/kernel/0711.0/1178.html]] 

Donc il ne reste plus maintenant qu'à compléter les parties qui font fonctionner les moteurs 3D des cartes. C'est cepandant plus facile à dire qu'à faire, puisque marcheu doit finir sa partie générique sur Gallium3D afin que les plus vieilles cartes, sans shaders de taille variable, puissent aussi gérer les requètes. 

Quelques sujets récents : 

* Nous cherchons quelqu'un pour écrire le test "Est-ce que les _notifiers_ en espace AGP marchent?" pour nouveau. Si le _notifier_ ne marchait pas, Nouveau devrait utiliser les _notifiers_ en espace PCI. pq et marcheu sont tout les deux occupés, mais ce patch pourrait pas mal aider. 
* notre politique de formatage de code est : indenter par des tabs de 8 caractères, plus des espaces en plus si l'indentation est inférieure à 8 caractères (par rapport au dernier tab de la ligne) 
* La documentation de Xv pour mmio-parse a été améliorée par hkBst. 

### Aide requise

Testez si Nouveau PPC marche pour vous (dans le cas où vous ayez ce type de matériel). 

De plus, regardez à la page « Testers wanted » pour les demandes qui peuvent avoir lieu entre les TiNDC. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

Et finalement un remerciement à Luc Vernerey pour nous avoir envoyé 2 cartes (GeForce 3 et 4) 

[[<<Édition précédente|Nouveau_Companion_29-fr]] [[Édition suivante >>|Nouveau_Companion_31-fr]] 
