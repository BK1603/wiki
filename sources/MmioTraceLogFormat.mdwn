# Mmiotrace raw log format


## The binary log format

Some very old versions of [[MmioTrace|MmioTrace]] dumped some version of a binary log format. You should forget about those, but if you can't, see if `mmio-convert` works for you. The binary format had several shortcomings. 


## The ascii log format

The modern raw [[MmioTrace|MmioTrace]] log is text and easily filtered with e.g. grep and awk. One record is one line in the log. A record starts with a keyword, followed by keyword dependant arguments. Arguments are separated by a space, or continue until the end of line. This is how the log format looks like: 
[[!table header="no" class="mointable" data="""
**explanation** | **keyword** | **space separated arguments **|||||||
read event  | R  | width (bytes)  | timestamp  | map id  | address  | value  | PC  | PID 
write event  | W  | width (bytes)  | timestamp  | map id  | address  | value  | PC  | PID 
ioremap event  | MAP  | timestamp  | map id  | physical  | virtual  | length  | PC  | PID 
iounmap event  | UNMAP  | timestamp  | map id  | PC  | PID 
marker  | MARK  | timestamp  | text 
version  | VERSION  | string 
info for reader  | LSPCI  | one line from `lspci -v` 
PCI address map | PCIDEV | space separated /proc/bus/pci/devices data ||
unk. opcode  | UNKNOWN  | timestamp  | map id  | address  | data  | PC  | PID 
"""]]

**Timestamp** is in seconds. **Address** and **physical** are physical addresses, **virtual** is a kernel virtual address. **Map id** is an arbitrary id number identifying the mapping that was used in an operation. **PC** is the program counter and **PID** is process id. PID is always zero as tracing user space memory events is not supported yet. 

For instance, the following awk filter will pass all 32-bit writes that target physical addresses in the range [0xfb73ce40, 0xfb800000[ 
[[!format txt """
awk '/W 4 / { adr=strtonum($5); if (adr >= 0xfb73ce40 && adr < 0xfb800000) print; }'
"""]]
Note, that to be able to use `mmio-parse` on the filtered output you need to re-add or preserve all MAP and PCIDEV records. You can do that easily by first filtering those into a separate file and then just running `cat maps.txt awk-filtered.txt | mmio-parse...` You can also use timestamps as unique (well, unique in practise) identifiers for events, when examining multiple filtrations of a log. 

For filtering you are likely interested in register offsets and not physical addresses. Lspci lists PCI memory resources in physical addresses. For Nvidia, the first resource, a.k.a BAR 0, is the MMIO register range. It is marked as non-prefetchable. Register offsets are relative to BAR 0. 
