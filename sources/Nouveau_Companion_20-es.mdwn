[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_20]]/ES/[[FR|Nouveau_Companion_20-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 26 de Mayo


### Introducción

Aquí estamos de nuevo con vosotros en el número 20 del TiNDC. 

Pq escribió una página de introducción a todos los temas importantes para la programación de drivers. Enlaza a otros documentos y debería proporcionar una visión general básica sobre qué hacer al desarrollar nouveau. 

Todavía no se ha tomado una decisión respecto a nuestra entrada en la Software Freedom Conservancy. El proyecto no fue objeto de debate en la reunión de Abril, pero estamos en la orden del día de la reunión de evaluación de Mayo, que será pronto. Obviamente, deberíamos saberlo, como mucho, en cinco días. 

Sin cambio respecto a nuestros problemas de infraestructuras. La cuota en SF ha sido excecida y todavía no existe otro servicio en marcha. Sin embargo, no dudéis en enviar volcados (especialmente de las tarjetas más viejas, más nuevas, SLI o quadro). 


### Estado actual

Parece que lentamente la rama _randr12_ va tomando forma. _Killertux_ informó de su éxito con una tarjeta `NV43`. Sin embargo, el ancho combinado de ambas pantallas no podía hacerse más ancha de 1280 píxeles, lo que (para visualizar en dos pantallas) es un poco escaso. Puesto que _Airlied_ estaba de vacaciones no ha habido más cambios a la rama. 

El trabajo prosigue en las tarjetas `NV1x`. _Matc_, _pmdata_ y otros llevaron a cabo más pruebas. Actualmente, las tarjetas fallan al emitir algunas intrucciones de punto de vista. _Marcheu_ está bastante seguro de que antes funcionaba, de acuerdo con sus notas y los registros de cambios de `git` en torno al 10 de marzo. Los comentarios del TiNDC #15 parecen corroborarlo. Ya que ahora mismo todas las tarjetas hasta la `NV3x` fallan al fijar `VIEWPORT_ORIGIN`, y antes funcionaban, un cambio guardado antes de esa fecha debe haber roto esta funcionalidad. _matc_ está todavía buscando la causa del fallo. 

La depuración del fallo (con `git-bisect`) es, sin embargo, difícil, ya que en torno a esa fecha se comenzaron a realizar cambios frecuentes en la `API` del `DRM`. De todos modos, _careym_ se ofreció a localizar la causa del fallo, aunque sin fortuna hasta ahora. 

_KoalaBR_ añadió finalmente un par de scripts más al CVS de sourceforge: 

* **crashdump** 
      * Recoge todos los datos relevantes sobre un fallo del servidor X11. Es similar a la herramienta nvidia-bug-report.sh. Por favor, pruébalo e informa a _KoalaBR_ qué tal va (o no va) en tu equipo. 
* **createdump** 
      * Descarga, compila, ejecuta y crea un archivo (listo para enviar por correo) de las pruebas de `renouveau`. Está pensado únicamente para usuarios noveles. Explica cada paso y comprueba si existen problemas. 
Tal como prometimos, ha comenzado la ingeniería inversa de las tarjetas basadas en `G84` que están en nuestras manos. Puede llevar cierto tiempo, ya que el diseño parece ser muy diferente al anterior. 

Hasta el momento, nuestros hallazgos parecen indicar que: 

* Hasta `NV50` se hace referencia a los objetos usando su desplazamiento en `RAMIN`. En `NV50`, los canales tiene sus propias tablas de hashes, y se señala a cada objeto en referencia a una dirección base por canal. 
* Ahora `PRAMIN PIC BAR` está paginada. Ya no se localiza simplemente al final de la vram en bloques de 512 Kb. 
Ya hemos añadido algunas instrucciones nuevas y objetos a renouveau para que los volcados en tarjetas G8x indiquen con menos frencuencia `NV50_TCL_PRIMITIVE_3D`. 

_hellcatv_ visitó el canal y nos explicó que por casualidad asistió a una charla en la que se explicaba otro esfuerzo para hacer ingeniería inversa de las tarjetas `G70` y `G8x` (N. de T.: con metodología _clean room_, es decir, documentación en implementación separadas). La persona tras él avanzó mucho y no conocía nuestro proyecto. Por lo que _hellcatv_ pidió si podía indicarnos su correo electrónico y mostró gran interés. _Marcheu_ se puso en contacto con él por correo electrónico. Si sale algo de esto, nuestro conocimiento sobre la tarjeta `G8x` puede mejorar drásticamente. 

Bueno. Oigo quejas por parte de cierto desarrollador sobre que no se informa a menudo acerca de `rules-ng` en el TiNDC. Aunque siento disentir sobre el asunto, ahora tenemos más información sobre el tema: 

_pq_ anunció que había comenzado a integrar su trabajo en `rules-ng` de nuevo en `mmio-parse`. Poco después del anuncio, _Thunderbird_ solicitó información adicional, ya que sigue investigando los registros para la activación y desactivación de la salida de TV en tarjetas Nvidia. Desgraciadamente, los parches de _pq_ no estaban todavía publicados en ese momento. Hasta esa fecha, _jrmuizel_ no tuvo tiempo para integrarlos, ya que los había recibido hacía escasas horas. Tras ponerse manos a la obra con la nueva versión, _Thunderbird_ hizo algunas preguntas sobre cómo funcionaba `rules-ng`, ya que sus cambios en la base de datos xml no aparecía en `mmio-parse`. 

La versión actual de `rules-ng` usa un paso intermedio que analiza el archivo xml y crea una libería en C a partir de él. Únicamente la información compilada en esta base de datos aparecerá en las interpretaciones de `mmio` (`mmio-parse`s). 

Mmm, he sido corregido. Obviamente no entendí bien. Pensó que el progreso había sido tan minúsculo que no merecía la pena informar sobre él. Bien, siento disentir de nuevo :) . 

Puesto que esta fue la primera vez que `rules-ng` se puso a prueba en condiciones "reales", se encontraron algunos fallos que fueron solucionados, se implementaron algunas funcionalidades que hacían falta marcas de documentación, y la vida nos sonrió de nuevo. 

Finalmente, _Thunderbird_ obtuvo los datos que necesitaba de los vaolcados y se embarcó a la aventura de descubrir más sobre los registros de salida de TV en tarjetas NVidia. 

_Darktama_ comenzó a trabajar en la incorporación de funciones 2D de las tarjetas `G8x`. Intentó hacer funcionar el establecimiento de modo en el `DRM` pero, por alguna extraña razón, no conseguía que el código hiciese lo que se esperaba. Tras alguna investigación, _Darktama_ lo hizo funcionar :) . Por lo que ya debería haber ya un 2D funcional. Tened en cuenta, sin embargo, que si se regresa al modo texto existen muchas posibilidades de que la pantalla esté trastocada. Estamos trabajando en ello. 

Ya apuntamos en un número anterior que no es posible simplemente hacer una copia del código de `nv` y que funcione. Es necesario adaptar el código al `DRM`, y partir de código ofuscado no es una verdadera opción... 


### Ayuda necesaria

Buscamos volcados de `MMio` de tarjetas en las que no funciona correctamente `glxgears`. 

Los scripts antes mencionados de _KoalaBR_ requerirían más pruebas, especialmente el script `crashdump` (volcado de errores). Por favor, indicad a _KoalaBR_ si os funcionan o no. 
