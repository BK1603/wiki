[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[TiNDC 2008|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_38]]/[[ES|Nouveau_Companion_38-es]]/[[FR|Nouveau_Companion_38-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau


## Édition du 6 avril 2008


### Introduction

Environ 3 semaines ont passé depuis la dernière édition et il est temps de vous informer du statut actuel. 

Manifestement, il y a un schéma général qui se répète tous les ans quand nous cherchons à obtenir des places pour le Google Summer of Code : l'activité n'apparait que si la date limite de soumission des propositions est proche (< 7jours). (NDT : procrastination ;-) ). La date a été repoussée au lundi 7 avril, vous avez donc encore toutes vos chances. Contactez nous si vous êtes intéressés. Puisque, cette année encore, nous essayons d'avoir des étudiants dans notre projet. Actuellement, sont interessés : 

* yeknom (Suspend / Resume pour les NV4x) 
* riverbank (implémentation simple de Gallium3D pour les puces NV2x) 
* support de XvMC (décodage vidéo accéléré via la carte)  
* Décodage générique des vidéos accéléré par le GPU. Tâche plus généraliste pas vraiment spécifique à Nouveau mais qui nous bénéficiera aussi. 
Stillunknown a reçu une donation de mycroes pour acheter une carte 8400GS, il peut maintenant travailler sur la gestion des modes des NV5x. En fait, je ne sais pas si Stillunknown apprécie ou non d'avoir du travail en plus :) 

Dans tous les cas, merci beaucoup à mycroes pour sa contribution généreuse. 

Les semaines autour de Pâques ont été peu animées en raison des vacances qui imposent des interactions sociales ainsi que nombres d'autres raisons personnelles. Donc sans plus attendre, passons au vrai contenu de ce TiNDC. 


### Statut actuel

Stillunknown possédant maintenant sa propre carte NV5x, il a commencé par s'informer du statut de ces dernières et par nettoyer et désobscurcir le code de gestion des modes hérité de nv. Le branchement à chaud des DVI fonctionne, mais il cherche surtout à trouver des informations sur les interruptions générées lors de ce branchement. Ce qui le conduit à enquêter sur les interruptions émises par la carte. 

Pq a mis à jour le statut pendant que ahuillet et stillunknown s'occupaient de la matrice de statut. Texture et Lumière ont été déclarées N/A pour au moins les NV4x étant donnée que sur ces cartes, elles sont gérées au travers des shaders. Ce qui est probablement vrai aussi pour les NV3x. 

Pq travaille toujours à rendre [[MmioTrace|MmioTrace]] prêt pour l'inclusion dans le noyau linux. Il y a encore quelques bugs, peut-être même dans la couche inférieure ftrace mais le tout avance bien. Il fonctionne avec un CPU seul et le noyau 2.6.25rc ( patchs pour le pilote nvidia [[http://www.nvnews.net/vbulletin/showthread.php?t=110088|http://www.nvnews.net/vbulletin/showthread.php?t=110088]] ). En SMP, on peut néanmoins rencontrer une erreur de type "recursive probe hit" voire un blocage complet.   
 A ce sujet d'ailleurs, il semble que le blob soit de nouveau MMiotraçable avec les pilotes 169.x et supérieurs.  Si vous voulez tracer des NV5x, utilisez des pilotes nvidia récents. 

Malc0, pour sa part, a expérimenté avec la gestion des modes dans le noyau. Il a même  implémenté un pilote framebuffer très basique (nouveaufb) pour les NV04-4x. Celui-ci permet d'obtenir une console et de démarrer X en utilisant le pilote fbdev. La gestion des modes est faite dans le noyau mais pour l'instant, il n'y a pas d'accélération.  
 Selon Malc0 : « il faut tout de même noter [...] que le code est effroyablement moche :-D, j'ai utilisé l'approche 'Quel est le minimum à faire pour obtenir quelque chose de grossièrement fonctionnel » Ce pilote est seulement un prototype et sera remplacé par quelque chose de meilleur par la suite. 

Passons à Marcheu qui vient juste de finir de monter son nouvel ordinateur avec la belle carte NV5x qui lui a été offerte (voir édition précédente). Avant de se pencher sur les NV5x, il veut néanmoins terminer 2 ou 3 choses : 

* finir avec les NV1x avant de laisser le code à p0g. Notamment, il cherche à avoir une architecture correcte. Glxinfo ne crashe d'ailleurs plus. La suite, c'est avoir les « state atoms » corrects. Une fois tout ça fini, p0g devrait être plus que capable de continuer. 
* Quelques corrections seront surement nécessaire pour que l'architecture des NV3x soit correctes. 
* et il sera enfin temps de coder pour les NV5x 
Maintenant que nous avons parlé de Marcheu, stillunknown, pq et malc0, intéressons nous à ce que fait Darktama. 

Et bien, il a écrit un petit programme pour parler directement aux NV5x. Et il est capable de rendre des triangles en ignorant le support inexistant actuellement du DRM pour ces cartes.  
 Ça pourrait sembler peu, mais c'est un premier pas : la preuve que nous (enfin, Darktama) pouvons amener le moteur 3D non seulement à effacer quelque chose (étape atteinte dans la dernière édition) mais également à afficher quelque chose. Ça n'était pas simple et nous pouvons lui taper dans le dos pour ça. :) 

Pour ne pas être accusé d'être paresseux, Darktama a continué son travail sur les NV4x. Ce qui ne prendra plus beaucoup de temps puisque la 3D est quasiment faite. Actuellement, il reste deux problèmes majeurs : La gestion de la mémoire n'utilise pas encore le TTM, ce qui signifie que les clients OpenGL (fonctionnels par ailleurs) finissent par crasher tôt ou tard. Le durée de vie dépendant du matériel et du client. Soyez rassuré tout de même, ça crashera souvent.  
 L'autre problème est qu'à chaque fois que nous effectuons un rendu, nous sommes obligés de vider le flot de commande dans le GPU. Ce qui est mauvais pour un certains nombres de raisons : 

* Les performances sont meilleures quand le GPU peut traiter un ensemble de commandes d'un coup 
* avec le TTM, il y aura beaucoup trop d'appels noyaux à notre goût 
Actuellement, quand nous effectuons le rendu d'une scène, le pilote va effectuer : effacement (jusqu'à 3 fois), vidage, rendu, vidage, inversion des tampons, vidage. Chaque vidage signifie un passage dans le noyau avec le TTM. Ce qui n'est évidemment pas une bonne solution et une réflexion plus poussée est nécessaire. Marcheu est certain qu'il est possible de faire différemment, le TTM conservant les objets plus longtemps. CE qui nécessite de la réflexion technique également.  
 Ce triple effacement nécessite quelques explications : il a a voir avec comment Gallium gère certains effacement. Il « outrepasse » le pilote et les effectue séparément pour chaque tampon avec des quadrilatères. Ce n'est pas terrible étant donné que le matériel Nvidia est capable de bien mieux. 

En ce qui concerne le TTM, si l'on exclue les problèmes ci-dessus, Nouveau est quasiment près. Il « suffirait » que quelqu'un s'y attaque (hem, quelques connaissances sur le pilote sont tout de même nécessaires) et branche les appels de Nouveau sur le TTM. 

Avant que vous commenciez à sauter de joie : il reste encore beaucoup de travail, avec des problèmes de notre côté (comme détaillé dans cette édition) et du côté de Gallium. 

Par exemple, en cherchant à rendre les réflexions, nous avons remarqué que ce n'est pas simple à faire avec Gallium. Rapidement : avec les cartes Nvidia, il est actuellement impossible d'obtenir les fonctionnalités de glClipPlane() sans passer par les shaders. Malheureusement, c'est un des axiomes de Gallium. La seule solution est alors de passer par une solution de repli logicielle qui n'est pas très performante (si vous êtes curieux, essayez neverball avec et sans réflexion pour voir la différence). Darktama a proposé un patch qui est en cours de revu par les Grands Gourous de Gallium. Plus de détails ici : [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-29.htm#1425|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-29.htm#1425]]  Autres sujets à mentionner : 

* Malc0 a corrigé le problème de l'éclairage des dalles des portables Apple. 
* Des tests sur le modesetting des NV5x et le rendu 3D ont été faits 
[[<<Édition précédente|Nouveau_Companion_37-fr]] [[Édition suivante >>|Nouveau_Companion_39-fr]] 
