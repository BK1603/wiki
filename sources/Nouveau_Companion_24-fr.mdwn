[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_24]]/[[ES|Nouveau_Companion_24-es]]/[[FR|Nouveau_Companion_24-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 21 juillet 2007

[!] **La traduction est finie, n'hésitez pas à relire et à corriger** 


### Introduction

À nouveau, bonjour à tous. Voici l'édition 24 de notre TiNDC. Actuellement, le développement semble vraiment s'accélérer avec ahuillet, darktama, pmdata, pq et même marcheu que l'on voit réapparaitre de plus en plus souvent (il annonce revenir en pleine forme d'ici une à deux semaines). 

Si ce rythme persiste, je devrais me poser la question de renommer le TiNDC en « Compagnon quotidien de Nouveau ». :) 

C'est quand même mieux que de devoir remuer ciel et terre pour avoir la moindre nouveauté. Je voudrais maintenant vous remercier pour votre intérêt dans notre projet. À peine 36 heures après la publication de la dernière édition, 1750 visites avaient été enregistrées, et 24 heures plus tard, nous étions à 2650. 10 jours plus tard, nous en sommes à ~5200 visites ! 


### Statut actuel

Darktama a poussé ses modifications dans le DRM et le DDX (branche nv50) afin d'avoir un support préliminaire des G8x. Comme, sur les cartes 8x00, la manipulation des objets utilise des adresses 64 bits à la place de 32 bits (et donc des objets plus gros), qu'il y a une meilleure abstraction et séparation entre les contextes OpenGL et 64 contextes différents/FIFO disponibles, avec un GPU capable de protéger la VRAM des accès CPU, des changements étaient nécessaires. 

Avant de pousser ces changements, darktama a testé l'idée d'un mode de compatibilité, c'est à dire d'utiliser des objets et commandes NV4x sur G8x. Malheureusement, ça n'a pas marché du tout, la carte se plaignant lourdement. 

Le DRM a besoin de savoir gérer ces nouveaux objets, d'où les patchs de Darktama. Des tests sur une deuxième G84 furent couronnés de succès, l'affichage 2D étant accéléré via EXA copy et les routines EXA solid. Pensez à utiliser l'option "MigrationHeuristic greedy" et préparez vous à rencontrer les problèmes mentionnés précédemment lors du passage en mode console texte. 

Malheureusement, ces patchs n'ont pas vraiment appréciés le mélange avec ceux, non encore poussés dans la branche principale, de Ahuillet, lequel a du rudement apprendre la résolution de conflit avec git. 

L'item suivant sur la TODO liste de Darktama est la suppression du codage en dur de la configuration PRAMIN des G8s/NV5x. Il pense finalement avoir compris comment faire et a l'intention de le prouver avec différents patchs. Le premier a déjà été inclus, d'autres sont à venir. 

Revenons en aux problèmes de DMA de Ahuillet, stillunknown a beaucoup aidé en testant diverses combinaisons de DRM et DDX. En premier lieu, Ahuillet réussi à obtenir un PCIGART fonctionnel pour Nouveau. Néanmoins, les tests ne montrèrent qu'un succès mitigé, avec un peu de tout : de blocages DMA (NV43, PCIe, 64bits) à des améliorations de la vitesse d'EXA (cartes < NV50) en passant par un Xv plus lent (transferts DMA comparés à un simple memcpy(), ce qui n'a pas vraiment de sens). 

Un peu de confusion se répandit parmi les développeurs et les testeurs mais des gens haut placé de chez X vinrent à la rescousse, pointant divers problèmes dans la programmation du DRM (comme l'utilisation de virt_to_bus() qui est loin d'être une bonne idée sur PPC ou x86_64). IDR (Ian Romarick) fit un peu de nettoyage dans le code, ce qui permit aux PPC de survivre à un démarrage de X, nettoyage rapidement interrompu par manque de temps. 

Benh (Benjamin Herrenschmidt) pris la suite de IDR et après deux jours de hacking, il réussit à obtenir quelque chose de fonctionnel pour les PPC. Il y a toujours un grand nombre d'erreurs de rendu (notamment les polices) et un gestionnaire de composition qui essaierait d'afficher des ombres obtiendrait des erreurs de blitting. (fil : [[http://lists.freedesktop.org/archives/nouveau/2007-July/000200.html|http://lists.freedesktop.org/archives/nouveau/2007-July/000200.html]]) 

Des tests préliminaires montrent que le DMA (PCI ou AGP) accélère EXA, mais qu'il est plus lent pour Xv, en ce sens qu'il consomme plus de CPU et met plus de temps pour afficher une image d'une vidéo. 

La tâche suivante de Ahuillet, jb17some et p0g a été de profiler Xv afin de trouver le goulot d'étranglement, dans l'espoir d'améliorer le DMA de Xv. Ce n'est pas très important que Xv DMA soit un plus lent que la simple copie CPU du moment qu'il libère ce dernier pour d'autres tâches (comme décoder l'image suivante). 

jb17some profila (avec oprofile) nouveau_drv dans cette utilisation. Il montra ainsi que la plupart du temps est passé dans [[NvPutImage|NvPutImage]]() et [[NvWaitNotifier|NvWaitNotifier]](). Les autres fonctions étant à peine visible. 

Et quand jb17some fournit un code source annoté par les outils d'oprofile, la dernière pièce du puzzle se mit en place : il devint évident que le pilote était occupé à attendre d'être notifié de la fin du transfert DMA. 

Quelques heures plus tard, Ahuillet réussi à faire fonctionner oprofile et put confirmer le résultat de jb17some. Et comme memcpy() n'attendait pas d'être notifié, il était nettement plus rapide que le DMA PCI. Lequel était plus lent que le DMA AGP, en ce sens que plus de temps est passé à attendre la notification de fin de transfert. 

D'autres données de profilage montrèrent ensuite qu'environ 50% du temps était dépensé dans copy_from_user() / copy_to_user(). Étant donné que Nouveau ne devrait pas passer très souvent en espace noyau et certainement pas avec « un gros tas de données » comme marcheu le dit si bien, ces résultats soulevèrent quelques interrogations. Le schéma d'appels de Oprofile montre que tout cela est lié à des fonctions internes du serveur X. EDIT : en réalité, cela résulte d'un test érroné, les copy_*_user peuvent être évitées en utilisant de la mémoire partagée. 

Pour tester rapidement, le *Wait fut supprimé et ensuite testé. Résultats : PCI est deux fois plus rapide qu'avant et l'AGP gagna environ 10%. memcpy() continue néanmoins à être plus rapide et le DMA PCI donne un rendu corrompu (ce qui était attendu vu que la synchronisation interne a été supprimée). nous devrions peut-être ajouter que xv n'est pas pour un utilisateur normal vu qu'il ne peut afficher au mieux qu'une vidéo à la fois sans donner de problèmes de rendus. 

Bref, beaucoup de confusions qui peuvent être résumé : 

* NV50 et +, Xv ne fonctionne pas encore 
* EXA est accéléré par le DMA (PCI ou AGP) 
* le notificateur de transfert DMA, et son attente ralentissent DMA 
Darktama n'a pas chômé lui non plus : il a fusionné les branches nv50 et randr-1.2 dans le dépôt git du DDX, et donc il n'a plus besoin de synchroniser son code sur 2 branches (randr-1.2 et master). Ainsi, si vous voulez tester avec une carte NV5x/G8x, utilisez la branche randr-1.2, nv50 est obsolète. 

En dehors de ça, Darktama a également ajouté quelques patches à la branche randr-1.2 qui améliorent les performances des NV5x/G8x avec d'autres valeurs que « greedy » pour l'option « [[MigrationHeuristic|MigrationHeuristic]] ». 

hughsie essaya de faire fonctionner Nouveau sur FC7, ce qui ne fut pas très facile, le DRM du noyau et celui de git étant désynchronisé. Il réussi néanmoins et découvrit que cela marchait plutôt bien (d'après gtkperf, [[http://hughsient.livejournal.com/29989.html|http://hughsient.livejournal.com/29989.html]]). Il promit d'inclure une nouvelle version dès que le DRM sera de nouveau synchronisé avec leur noyau. 


### Aide requise

Nous apprécierions que des propriétaires de 8800 testent le pilote actuel et nous fassent part du résultat. Nous n'avons actuellement que deux cartes G84 pour développer et tester, et le retour d'autres utilisateurs de ces cartes serait bienvenue. Note : utilisez la branche randr-1.2 et faire remonter les résultats à Darktama. 

Et nous aurions besoin de dump [[MmioTrace|MmioTrace-fr]] pour les cartes NV41, NV42, NV44,NV45, NV47,NV48 et NV4C. Faites vous connaitre sur le canal si vous pouvez nous aider. 

Si vous n'avez pas froid aux yeux, vous pouvez tester les patchs de ahuillet : git://people.freedesktop.org/~ahuillet/xf86-video-nouveau et lui donner le résultat. Soyez prêt aux problèmes, bogues et autres crash, le code étant encore instable et en constante modification/amélioration. 

[[<<< Édition précédente|Nouveau_Companion_23]] | [[Édition suivante >>>|Nouveau_Companion_25-fr]] 
