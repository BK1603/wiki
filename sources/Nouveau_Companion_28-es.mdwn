[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_28]]/[[ES|Nouveau_Companion_28-es]]/[[FR|Nouveau_Companion_28-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe Irregular sobre el Desarrollo de Nouveau


## Edición del 14 de octubre


### Introducción

Volvemos con el número 28 sobre nouveau, el proyecto para crear un controlador libre para tarjetas NVidia. 

No, no estamos trabajando solos para tener un driver. Además de obtener ayuda, también la prestamos a otros proyectos. Haiku en lo referido a la entrada de cosas y MMioTrace en la de salida, son algunos ejemplos, y parece que durante las dos últimas semanas hemos visto el nacimiento de otro proyecto: el Soporte de gráficos en la PS3. 

Se inició cuando [[IronPeter|IronPeter]] entró en el canal y preguntó si teníamos alguna idea sobre el desarrollo en PS3 y cómo abordar la cuestión de las tarjetas NVidia en ese caso. Le proporcionamos algunas referencias (aunque tuvimos que reconocer que no podríamos ayudar demasiado con cuestiones específicas de PS3, ya que ninguno de los desarrolladores tiene una) y durante los siguientes días regresó solicitando más información sobre cómo funcionan las tarjetas NVidia, cosa que hicimos encantados. 

Tras algún esfuerzo, finalmente logró hacer funcionar los blits sencillos en 2D, superando una grave limitación de Linux en la PS3. Pero compruébalo por tí mismo: 

[[http://forums.ps2dev.org/viewtopic.php?t=8364|http://forums.ps2dev.org/viewtopic.php?t=8364]] 

Todo el trabajo más duro fue hecho por [[IronPeter|IronPeter]], y nosotros simplemente le proporcionamos algunas pistas técnicas. 

Al leer este número probablemente te des cuenta de que no se comenta demasiado sobre la parte 3D del controlador. Se debe sobre todo al hecho de que es necesario coger el toro por los cuernos y usar TTM (el gestor de memoria que debería estar ya "acabado", tal como mencionamos en el anterior número). 

Y un segundo "problema" es la nueva arquitectura de controladores "Gallium3D". 

Está basado en gran medida en nuevas funcionalidades (como programas de vértices, shaders, etc.) e intenta aliviar mucho el trabajo de quien escribe el controlador llevando la infraestructura de apoyo a Mesa. Naturalmente, los controladores antiguos y los nuevos no se parecen mucho. Gallium3D es lo último de lo último, y todavía está sujeta a algunas revisiones hasta lograr una API estable. Además, Gallium3D depende en esas funcionalidades recientes de las tarjetas. Escribir un controlador para tarjetas más antiguas no es tan fácil. 

Por lo que nos encontramos de nuevo en estado de espera. Esta vez, sin embargo, tenemos un plan: lograr que el 2D funcione en todas las tarjetas, hasta las NV4x, y hacer, posiblemente, un primer lanzamiento ("release"). El lanzamiento habría que tomárselo con cierto márgen, ya que es posible que se rompa la API binaria tras el mismo. 


### Estado actual

De vuelta a nuestro proyecto básico, donde también asistimos a bastantes avances. En estos momentos apuntamos a un controlador funcional en 2D (incluído EXA) para todos los modelos, desde la NV04 a las NV4x. Desgraciadamente, NV5x es cosa aparte y puede llevar algo más de tiempo. Sin embargo, hasta el 5-10-2007, la rama principal de desarrollo no funcionaba en absoluto en las NV5x / G8x, pero tras algunas pruebas y la bisección hecha por KoalaBR, stillunknown localizó y solucionó un error, que nos trajo un 2D funcional. Desgraciadamente, si se realiza un cambio al modo texto, se bloquea la pantalla, lo que hace bastante complicado hacer pruebas en un controlador con errores. 

En NV04, ahuillet luchó duro para hacer funcionar las X. Sus primeros intentos se encontraron con la indiferencia de la tarjeta. Esta simplemente no reaccionaba en absoluto. Justo cuando se iba a dar por vencido, matc le señaló algunos supuestos erróneos que había asumido. Además de ello, existía un problema de hardware con la placa base que resultaba en fallos de inicialización de hardware aleatorios tanto con la NV04 como con una NV11. 

Así, ahuillet tiró a la basura su código, volvió a empezar, hizo pruebas, y finalmente tuvo algún avance: sus métodos por software eran llamados por la tarjeta y las instrucciones inválidas producían las interrupciones de error esperadas. 

Lo siguiente es la implementación de un par de métodos por software. Estos son instrucciones que, cuando se envía a la tarjeta, hacen que esta pida al DRM que las gestione. NV04 implementa unos cuantos métodos por software relacionados con la inicialización, tal vez debido a algunas decisiones de diseño. Esos métodos simplemente establecen una bandera en las instancias de los objetos, y no son, por tanto, demasiado complicados de escribir. Cuando es preciso manejar un método por software, la tarjeta genera una interrupción determinada. 

Puso el código en su repositorio de git privado y espera integrarlo en el repositorio principal "pronto" (en los próximos días). Antes, ahuillet necesita resolver un último ausnto: 

X se inicia, pero es más lento que una tortuga, ya que la tarjeta genera una tormenta de interrupciones STATE_INVALID. Puede estar relacionado con cómo manejamos las interrupciones (se hizo un pequeño cambio por parte de ahuillet y darktama), pero es más probable que se deba a cualquier otra causa (tal vez debe señalarse el estado de la tarjeta). Se está investigando. Y, antes de que nos olvidemos, en NV04 también funciona la superposición (overlay). 

pmdata intentó eliminar los problemas existentes al usar glxgears con ventanas de tamaño distinto al predefinido. Debido a otros trabajos en marcha tuvo que abandonar la tarea por el momento. 

pq ha estado intentando hacer funcionar NV20-demo ([[http://cgit.freedesktop.org/~pq/nv20_demo/|http://cgit.freedesktop.org/~pq/nv20_demo/]]). Hizo uso de MMio para conseguir un trazado y empezó a analizarlo. Para hacer más llevaderos sus esfuerzos de depuración inició la escritura de un programa para volver a reproducir una secuencia trazada con mmio-trace. Esta herramienta toma un volcado de MMioTrace y lo vuelve a ejecutar línea a línea. Así se puede vér qué efecto tiene cada escritura en un registro. Eso, y algo de prueba y error, redujeron el tamaño del registro (log) de 60MB a 4500 líneas. No fue una tarea sencilla, ya que cada ejecución de las trazas necesitaba un reinicio de la máquina. Pero, ya que el estado de la tarjeta sobrevive incluso un reinicio en frío para apagados de menos de 30 segundos, la tarea llevó su tiempo. 

Al final, Marcheu recordó la razón mientras sucedía el problema: No había código para la incialización de contexto PGRAPH en el DRM, ya que nadie lo había escrito por el momento. 

Tras algunas pruebas, logró hacer funcionar la inicialización y también mostrar triángulos (con NV20_demo). El siguiente paso era hacer funcionar glxgears. A modo de solución rápida, pq parcheó MESA para que pensase que su NV28 era en realidad una tarjeta NV1x, lo que funcionó, dentro de las mismas limitaciones que tienen las NV1x (comentamos más cosas sobre esto antes y en el número anterior). El parche no se llegó a integrar nunca, ya que es una simple chapuza y es necesario realizar una implementación en toda regla para el controlador. 

Sin embargo, la inicialización significa una correcta inicialización para cada contexto, y eso requirió cambios estructurales profundos en el DRM, que produjeron que cesase el funcionamiento de todas las tarjetas NV2x, exceptuando las NV28/NV25. En ese momento, malc0_ ofreció una serie de parches que adecentaban bastante la inicialización de las NV2x: [[http://people.pwf.cam.ac.uk/sb476/nouveau/nv20pgraphctx/|http://people.pwf.cam.ac.uk/sb476/nouveau/nv20pgraphctx/]] lo que devolvió al funcionamiento normal a todas las tarjetas NV2x. Otro resultado de la combinación de todos los parches es que las tarjetas NV2x realizan de forma automática el cambio de contexto y ya no necesitan la intervención manual. 

Hay más información disponible sobre la inicialización de contexto de las NV2x aquí: [[http://nouveau.freedesktop.org/wiki/Nv20GraphInit|http://nouveau.freedesktop.org/wiki/Nv20GraphInit]] 

El trabajo con las NV20 se reveló complicado, ya que pq siempre acababa con un cuelgue de la cola de DMA muy pronto en todo el proceso de inicialización de las X. 

Al comentar sus problemas con marcheu, se pidió a pq que no se produjese el almacenamiento de los notificadores en la memoria AGP sino en la memoria de la tarjeta. Esto funcionó y mostró que pq tiene algunos problemas con el AGP: los notificadores situados en esa región de memoria no funcionan (eso pasa con algunas placas base). (Para una explicación sobre los notificadores mírense dos últimos números). 

Marcheu y JKolb trabajaron duro para hacer funcionar las NV3x. Primero intentaron hacer que funcionase NV30_demo (que hace la visualización de triángulos coloreados, simples y texturados). Se vió que era bastante complicado debido a algunos errores de inicialización. Tras copiar el código tal cual aparece en nv, funcionaba algo mejor, pero se bloqueaba, tras la ejecución de unas pocas instrucciones FIFO, produciendo un INVALID_COMMAND. 

Se encontró un buen número de errores, que fueron solucionados, y jkolb logró el siguiente objetivo: mostrar un triángulo. Pero siempre era negro (y ya que el fondo también era negro, el primer diagnóstico fue: "no se muestra nada" :) ). El siguiente paso lo dió marcheu: un triángulo coloreado. Para no quedarse atrás, jkolb añadió las texturas al lote. Lo siguiente esa entonces hacer funcionar EXA en la NV30. De nuevo, marcheu lo consiguió de nuevo con ayuda de jkolb y luego probó el rendimiento con un escritorio que usase un gestor con composición, además del rendimiento usando "[[MigrationHeuristics|MigrationHeuristics]] Always" (debería ser más rápido con EXA) frente al que se obtenía con "[[MigrationHeuristics|MigrationHeuristics]] Greedy" (más rápido si EXA no está completamente acelerado). 

El resultado es que EXA en NV3x no es utilizable todavía, debido al rendimiento y a que todavía faltan algunas rutinas (además de tener algunos fallos). 

Desafortunadamente, todavía no está listo y, por tanto, Marcheu y JKolb decidieron investigar el asunto. 

Algunos temas más cortos: 

* Algunas rutinas memcpy() se convirtieron a funciones en línea (inline), para 
      * obtener mejoras de velocidad apreciables. 
* Marcheu investigó de nuevo algunos errores de orden de bytes (endianess). 
      * Algunos resultaron difíciles de localizar pero, con la ayuda de BenH, marcheu logró hacer los cambios para solucionarlos. Desgraciadamente, el usuario que informó de esos problemas en un PowerBook tuvo un fallo de disco, por lo que no sabemos a ciencia cierta si el arreglo funciona. Es más, parece que se ha colado una opción de compilación exclusiva de X86/x64_86 en el makefile (para las funciones en línea de más arriba). 
* Para hacer más fácil de mantener el controlador para NV5x, GrowlZ asumió la 
      * tarea de desofuscar el controlador que "robamos" del controlador nv de las X11. Darktama lo aclaró algo en las partes en que era necesario adaptarlo a nuestra infraestructura de DRM. 
* pq trabajó en la creación de documentación a partir de su base de datos 
      * rules-ng. Se pueden ver los resultados aquí: [[http://people.freedesktop.org/~pq/rules-ng/|http://people.freedesktop.org/~pq/rules-ng/]] Cuando ahuillet lo vió, prometió documentar también los resgistros de Xv. 
Entonces, ¿qué nos falta para tener un controlador 2D aceptable?: 

* es necesario implementar EXA para NV10 
* se debe calmar la tormenta de irq en NV04 
* NV20 usará EXA para NV10 
* EXA para NV30 necesita funcionar más rápido (con algunas funciones de respaldo 
      * que deberían acelerarse, no resolverse por software) 
* EXA para NV40 funciona, pero no tan rápido como debería 
* el establecimiento del modo texto para la NV50 para poder tener un controlador muy básico 
      * (sin Xv todavía) 

### Ayuda necesaria

Ahuillet solicita ayuda para probar su código para NV04. Y si alguien quiere escribir documentación, por favor, poneos en contacto también con él. Os proporcionará la informacion que os hará falta, pero él no tiene en estos momentos tiempo para hacerlo por sí mismo (es una buena tarea para principiantes). 

Si tienes un sistema PPC, por favor, prueba el controlador e informa del resultado a marcheu. 


### Agradecimientos

De nuevo, somos objeto de donaciones de hardware. Por lo que, en nombre del proyecto Nouveau, Pekka Paalanen (pq) querría agradecer a Ville Herva, de Vianova Systems Finland Oy, su donación de una tarjeta GeForce 3 y otra GeForce 256. ¡¡¡Muchas gracias!!! 
