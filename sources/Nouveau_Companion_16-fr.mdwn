[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_16]]/[[ES|Nouveau_Companion_16-es]]/FR/[[RU|Nouveau_Companion_16-ru]]/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau / The irregular Nouveau-Development companion


## Édition du 17 mars 2007


### Introduction

Que dire ? Manifestement nous avons fait deux erreurs dont le résultat nous a pris par surprise : 

1. Dans le dernier TiNDC, nous avons lancé un appel pour des dumps REnouveau spécifiques 
1. Nous avons ajouté une news qui pointaient vers une liste (générée par un script de JussiP) résumant les besoins en dumps (carte déjà « dumpées », carte « non-dumpées », vieux dumps). 
Les choses évoluèrent alors de façon inattendue. Digg.com s'empara de l'affaire, de même que linuxfr.org et la demande fut relayée sur le forum Ubuntu. Notre canal fut alors submergé de questions sur la façon de faire un dump et de nous l'envoyer. Pq, pmdata et marcheu s'échinèrent à répondre mais l'affluence était telle qu'ils finirent par rendre les armes. Marcheu fut le premier à disparaître et nous ne le revîmes plus (jusqu'à ce jour tout au moins ;-). Pq et pmdata finirent également par abandonner en annonçant se replonger dans le développement. 

Bref, nous ne savons pas vraiment quoi dire excepté un grand **Merci !**. La volonté de la communauté à nous aider ne lasse pas de nous surprendre. Au 12 mars 2007, nous avons 427 dumps en attente pour un grand nombre de cartes différentes, y compris des 8800. 

Si vous souhaitez participer au Google Summer of Code 2007, allez lire la page [[SoC|SoC-fr]] et la section demande d'assistance plus bas sur cette page et contactez nous avant le 24 mars. 


### État actuel du projet

Quelque part durant les deux dernières semaines, le script d'upload depuis gmail vers sourceforge a arrêté de fonctionner. Cela devint évident quand la vague de dumps commença à arriver. Kmeyer, auteur du script, semblait penser que le problème venait d'une bande passante trop faible chez lui. Marcheu récupéra alors le script, le mis en place sur sa machine et 4 jours après la découverte du problème, la mise à jour des dumps se remis à fonctionner. Le script ne fonctionnait néanmoins pas correctement et seul une pette fraction des dumps était correctement traitée. Kmeyer recommença donc à débugguer le script et remarqua que le problème survenait avec certains clients de courriel qui généraient des entêtes assez étranges. Un peu de travail plus tard, le script se remis enfin à fonctionner correctement (d'où les 426 nouveaux dumps). 

Ceci devenait urgent depuis que JussiP avait crée une page ( [[http://users.tkk.fi/~jpakkane/ren/|http://users.tkk.fi/~jpakkane/ren/]] ) qui reprend et résume le statut du dossier des dumps sur sourceforge. Sans changement pendant un certains temps, la note en première page disant que nous croulions sous les dumps semblait suspecte :). Enfin, depuis le 15 mars, la page de statut devrait refléter l'état réel des dumps. 

Avec l'afflux de dumps, quelques problèmes apparurent avec REnouveau : sur certains systèmes PCI-express, il aura de grandes chances de crasher. Un séance de débuggage révéla que les derniers pilotes nVidia modifient les zones mémoires des registres IO à la volée, REnouveau continuant à essayer de lire l'ancienne zone mémoire qui évidemment n'est plus valide. Marcheu a essayé de corriger ce problème, sans succès jusqu'à présent. Pour le moment, nous ne pouvons que vous conseiller de réinstaller des pilotes plus anciens (87.76 ou moins) pour éviter ce problème. 

Des utilisateurs rapportèrent également des bogues openGL ("Latest OpenGL error: xxxx") qui dans certains cas peuvent conduire à de graves erreurs dans le pilote nVidia (Xid:...). Pmdata, KoalaBR et pq s'attaquèrent à corriger les tests de façon à ce que ces erreurs n'apparaissent plus. jb17bsome enquêta quand à lui sur les raisons des erreurs Xid et découvrit que dans certains cas, essayer de trouver un objet dans RAMIN via INSTANCE_MEM() conduisait à cette erreur. À l'heure actuelle, ce problème est toujours irrésolu mais marcheu et jb17some poursuivent leurs recherches. 

Nous avons maintenant une catégorie dédiée dans le bugzilla de freedesktop. Si vous constatez des problèmes avec notre pilote (en 2D ou en 3D), envoyez nous un rapport par ce biais. Vous pouvez également le mentionner sur le canal IRC, ça ne fera pas de mal, mais n'espérer pas une réponse ou une solution rapidement même si les développeurs le noteront. Bugzilla est disponible à cette adresse : [[https://bugs.freedesktop.org/|https://bugs.freedesktop.org/]] . 

Airlied a synchronisé la branche randr1.2 avec la branche principale (master) et ses corrections. Malgré tout, ses tests révélèrent des régressions sans que l'on puisse les relier à la synchronisation ou à ses propres développements. Airlied manquant de temps en ce moment, le développement s'en ressent (tests et corrections) mais on note quand même quelques progrès. 

Le 9 mars 2007, nVidia a rendu publique la version 1.99 de ses pilotes nv pour Xorg. Les G80 sont maintenant gérées. Peu après la sortie, darktama, Thunderbird et marcheu examinèrent le code pour en extraire différentes informations. Toutefois, rendre le code intelligible et l'intégrer à notre pilote devraient nécessiter pas mal de travail. 

Concernant [[MmioTrace|MmioTrace-fr]], toutes les branches ont été fondues dans la branche stable qui devraient contenir toutes les fonctionnalités désirées. 

Thunderbird et Skinkie essayent de leur côté de découvrir le fonctionnement des sortie TC des cartes nVidia. Quelques registres (positionnement de l'image, bordures, clignotement) ont déjà été retrouvés mais il reste encore beaucoup à faire. En tant que mainteneur de nvclock, Thunderbird possède déjà des connaissances sur les puces nVidia. Il s'intéresse aux données et tables issues du BIOS. De son côté, Skinkie préfère utiliser les donner issues de [[MmioTrace|MmioTrace-fr]]. 

pmdata et jwstolk ont rencontré de sérieux problèmes avec glxgears et les cartes NV15. Dès que la première commande est envoyé à la carte à travers le FIFO, le système crashe. Réduire la quantité de commandes envoyées ou ajouter un délai (NOP's) dans le FIFO est insuffisant ; le système fonctionne uniquement quand le FIFO reçoit la commande _NOP_ (hmm, ça ne crashe pas, mais bon, une No Operation ne donne pas grand chose au niveau du rendu :) ). Travail toujours en cours. 

Nous sommes également en train d'essayer de créer un outil pour automatiser la création de dumps REnouveau et faciliter la vie au débutants. Une première version expérimentale est disponible : [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]] , si vous l'essayez, n'oubliez pas de remonter vos impressions/rapports de bugs sur le fonctionnement à KoalaBR. 


### Demande d'assistance

Référez vous à la page [[http://users.tkk.fi/~jpakkane/ren/|http://users.tkk.fi/~jpakkane/ren/]] pour avoir l'état des dumps. Nous avons seulement besoin de dumps pour les cartes marquées en rouge. Nous serions très intéressés par des dumps de configuration SLI (n'oubliez pas de préciser ce détail dans le sujet de votre mail). Les possesseurs de G80 volontaires pour tester des patchs sont les bienvenus. 

De même pour les dumps [[MmioTrace|MmioTrace-fr]], ils sont appréciés. 

Nous avons besoin de personnes pour répondre aux questions des débutants sur le canal IRC. Le raz de marée mentionné en introduction nous en a réellement fait prendre conscience. Les réponses pouvant généralement être trouvé dans la [[FAQ|FAQ-fr]] ou sur le Wiki. 

SI vous voulez prendre part au Google Summer of Code, contactez marcheu. Nous avons des places disponibles sous l'égide de xorg. L'idéal étant que vous trouviez vous-même ce sur quoi vous voulez travailler. Différentes idées peuvent être trouvées sur la page de [[ToDo|ToDo]]-fr. Si vous n'avez pas d'idée, nous pourrons sûrement en trouver une ou deux. Une page dédiée ne devrait pas tarder à apparaître sur le wiki (NDT voir [[SoC-fr|SoC-fr]]). 

Pour terminer, Maximi89 serait très intéressé pour traduire les TiNDC ainsi que le wiki en espagnol, et il apprécierait d'avoir de l'aide d'autres personnes.   
 **NDT : Je lance le même appel à contribution pour la traduction française, d'autant plus que je serais offline pour quelques semaines**.  
 Plus généralement, nous aimerions pouvoir fournir des pages traduites, ne soyez donc pas timide et allez y. Et si vous souhaitez traduire le TiNDC, je peux vous faire parvenir le brouillon un jour avant sa parution, ce qui permettra à la traduction d'arriver plus vite. 

Et quand je vois que nous commençons à avoir une traduction française, je me demande où sont passés mes compatriotes Allemands ??? ;) 

Voilà, c'est fini pour cette fois, j'espère que vous avez apprécié la lecture. 

[[<<< Édition précédente|Nouveau_Companion_15]] | [[Édition suivante >>>|Nouveau_Companion_17-fr]] 
